#! /usr/bin/env bash
declare -A SERVER_ENDPOINTS
declare -A SERVER_PUBLIC_KEYS
declare -A SERVER_LOCATIONS
declare -a SERVER_CODES

RESPONSE="$(curl -LsS https://api.mullvad.net/public/relays/wireguard/v1/)" || die "Unable to connect to Mullvad API."
FIELDS="$(jq -r 'foreach .countries[] as $country (.; .; foreach $country.cities[] as $city (.; .; foreach $city.relays[] as $relay (.; .; $relay.hostname, $country.name, $city.name, $relay.public_key, $relay.ipv4_addr_in, $relay.multihop_port)))' <<<"$RESPONSE")" || die "Unable to parse response."

LINES=$(echo "$FIELDS" | awk 'NR%6{printf "%s ",$0;next;}1')

echo "$LINES"
