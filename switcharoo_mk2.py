#!/usr/bin/env python3

import os
import re
import yaml
import json
import random
import logging
import argparse
import requests
import subprocess

from pathlib import Path

"""
Script for managing wireguard connections, specifically for generating random tunnelled connections.
Not using some PyPI library, because my tin-foil-hat is too thicc.

Useful Mullvad docs:
https://mullvad.net/en/help/wireguard-and-mullvad-vpn/
"""

###############
# General TODOs
# #############

# Post-quantum security with pre-shared key
# https://mullvad.net/en/blog/2023/4/6/stable-quantum-resistant-tunnels-in-the-app/
# https://www.wireguard.com/protocol/

########
# Config
########

# https://api.mullvad.net/app/documentation/
SERVER_API_URL = "https://api.mullvad.net/public/relays/wireguard/v1/"

# Store generated tunnel config files here
CONFIG_PREFIX = Path('/etc/wireguard')

# Write local server list cache to this file
SERVER_FILE = Path('/etc/mullvad/servers.json')

# Private key file
PRIVATE_KEY_FILE = Path('/etc/mullvad/privkey')

# Don't connect to these countries
BLACKLIST = ['USA', 'UK', 'New Zealand', 'Canada', 'Australia', 'Israel', 'Norway']

# Whom do we ping to check that we're online?
PING_TARGET = '9.9.9.9'

# Kill connectivity if VPN connection dies, avoid leaks
KILLSWITCH = """PostUp  =  iptables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT && ip6tables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT
PreDown = iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show  %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT && ip6tables -D OUTPUT ! -o %i -m mark ! --mark $(wg show  %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT"""

# ANSI colors and styles
t_colors = {
    'header': '\033[95m',
    'blue': '\033[94m',
    'green': '\033[92m',
    'warning': '\033[93m',
    'fail': '\033[91m',
    'endc': '\033[0m',
    'bold': '\033[1m',
    'underline': '\033[4m',
}

# Pattern for turning wg output space-separated word into underscore-separated ones
to_underscore_pattern = '(?<=[a-z])(?<!:)(?<!n) '

#########
# Classes
#########

class Location:
    def __init__(self, country_name:str, city_name:str):
        self.country = country_name
        self.city = city_name

class InterfaceStatus:
    def __init__(self, name:str, online:bool, port:int, endpoint:str,
                 ingress_loc:Location, egress_loc:Location):
        self.name = name
        self.online = online
        self.listening_port = port
        self.ingress_endpoint = endpoint
        self.ingress_location = ingress_loc
        self.egress_location = egress_loc

    def __str__(self):
        # Interface name
        if self.name is None:
            iface_str = f"Name: {t_colors['fail']}Unknown{t_colors['endc']}"
        else:
            iface_str = f"Name: {t_colors['green']}{self.name}{t_colors['endc']}"
        # Online
        if self.online:
            online_str = f"Online: {t_colors['green']}{self.online}{t_colors['endc']}"
        else:
            online_str = f"Online: {t_colors['fail']}{self.online}{t_colors['endc']}"
        # Listening port
        if self.listening_port is None:
            port_str = f"Listening port: {t_colors['fail']}Unknown{t_colors['endc']}"
        else:
            port_str = f"Listening port: {t_colors['green']}{self.listening_port}{t_colors['endc']}"
        # Ingress Endpoint
        if self.ingress_endpoint is None:
            enpoint_str = f"{t_colors['fail']}Unknown{t_colors['endc']}"
        else:
            endpoint_str = f"{t_colors['green']}{self.ingress_endpoint}{t_colors['endc']}"
        # Ingress location
        if self.ingress_location is None:
            ingress_str = f"Ingress Location: {t_colors['fail']}{self.ingress_location.country}, {self.ingress_location.city}{t_colors['endc']}"
        else:
            ingress_str = f"Ingress Location: {t_colors['green']}{self.ingress_location.country}, {self.ingress_location.city}{t_colors['endc']}"
        # Egress location
        if self.egress_location is None:
            egress_str = f"Egress Location: {t_colors['green']}{self.egress_location.country}, {self.egress_location.city}{t_colors['endc']}"
        else:
            egress_str = f"Egress Location: {t_colors['green']}{self.egress_location.country}, {self.egress_location.city}{t_colors['endc']}"

        out_str = f"""{iface_str}
{online_str}
{port_str}
{ingress_str}, {endpoint_str}
{egress_str}
        """
        return out_str

###########
# Functions
###########

def check_perms() -> bool:
    """
    Check that we have sufficient access to execute (root).
    """

def select_random_srv(servers:dict) -> (dict, str):
    """
    Select a random server from the available ones.
    :param servers: Parsed server json, dict
    :return: Relay dict, country name string
    """
    # Create list of eligible servers, each relay has equal chance to be selected
    relay_dict_list = []
    for country in servers['countries']:
        if country['name'] in BLACKLIST:
            continue
        for city in country['cities']:
            for relay in city['relays']:
                relay_dict_list.append(relay)

    relay_dict = random.choice(relay_dict_list)

    # Do second pass to check which country this one is from
    country_str = ""
    for country in servers['countries']:
        for city in country['cities']:
            if relay_dict in city['relays']:
                country_str = country['name']

    return relay_dict, country_str

def select_relay_by_country(servers:dict, country_code:str) -> dict:
    """
    Select a server in a specific country.
    :param servers: Parsed server json, dict
    :return: Relay dict, country name string
    """
    relay_dict_list = []
    for country in servers['countries']:
        if country['code'] != country_code:
            continue
        for city in country['cities']:
            for relay in city['relays']:
                relay_dict_list.append(relay)

    relay_dict = random.choice(relay_dict_list)

    country_str = ""
    for country in servers['countries']:
        if country['code'] == country_code:
            country_str = country['name']

    return relay_dict, country_str

def fetch_server_dict(api_url:str) -> dict:
    """
    Use the mullvad API to feth a list of all servers.
    :param api_url: URL of Mullvad server list API
    """
    logging.info(f"Fetching server data from {api_url}")
    req = requests.get(
        SERVER_API_URL
    )
    return req.json()

def update_server_list(args):
    """
    Update the local server list file.
    :param args: Argparse args
    """
    servers = fetch_server_dict(api_url=SERVER_API_URL)
    logging.info(f"Updating server list file at {args.server_file_path}")
    with open(args.server_file_path, mode='w', encoding='utf-8') as server_file:
        server_file.write(json.dumps(servers))

def parse_existing_conf():
    """
    Read an existing config file (source) to fetch needed details such as the
    privkey.
    """
    pass

def bring_up(args):
    """
    Bring a new interface up.
    """
    # Fetch private key
    privkey = read_privkey_file(args.privkey_path)

    servers = read_server_file(args.server_file_path)
    # Select ingress
    if args.ingress == "random":
        ingress, i_country_name = select_random_srv(servers)
    else:
        ingress, i_country_name = select_relay_by_country(servers, args.ingress)
    logging.debug(f"Selected ingress relay: {ingress['hostname']} - {i_country_name}")

    # Select egress
    if args.egress == "random":
        egress, e_country_name = select_random_srv(servers)
    else:
        egress, e_country_name = select_relay_by_country(servers, args.egress)
    logging.debug(f"Selected egress relay: {egress['hostname']} - {e_country_name}")

    # Generate config
    config_name = f"{re.sub('[0-9]|-', '', ingress['hostname'])}-{re.sub('[0-9]|-', '', egress['hostname'])}"
    config = generate_wireguard_conf(args, privkey, ingress, egress)
    write_config_file(args, config, config_name)

    # Take down existing config
    put_down(args)

    # Apply config
    apply_cmd = f'wg-quick up {CONFIG_PREFIX}/{config_name}.conf'
    apply_res = subprocess.run(apply_cmd, capture_output=True, shell=True)
    if apply_res.returncode == 0:
        logging.info(f"Successfully brought up wireguard interface {config_name}")
        if not args.quiet:
            print(f"Created connection {config_name} {t_colors['warning']}{i_country_name}{t_colors['endc']} --> {t_colors['warning']}{e_country_name}{t_colors['endc']}")

def put_down(args) -> bool:
    """
    Take down the current active interface.
    """
    # Find name of current interface
    wg_show_cmd = 'wg show'
    show_res = subprocess.run(wg_show_cmd, capture_output=True, shell=True)
    iface_text = show_res.stdout.decode('utf-8')

    # Handle condition where there is no iface
    if iface_text == '':
        logging.info("No active interface found, proceeding.")
        return True

    # Take it down
    iface_name = iface_text.split('\n')[0].split(' ')[1]
    down_cmd = f'wg-quick down {iface_name}'
    down_res = subprocess.run(down_cmd, capture_output=True, shell=True)

    if down_res.returncode == 0:
        logging.info(f"Successfully brought down wireguard interface {iface_name}")
        return True
    else:
        logging.error(f"Failed to bring down wireguard interface {iface_name}")
        return False

def read_server_file(path:Path) -> dict:
    """
    Read file containing API output and relay details.
    """
    logging.info(f"Reading server file cache at {path}")
    with open(path, mode='r', encoding='utf-8') as sf:
        return json.load(sf)

def read_privkey_file(path:Path) -> str:
    """
    Read file containing wireguard private key.
    """
    # TODO: Check file permissions and complain if they are not sufficiently
    # paranoid.

    logging.info(f"Reading private key file at {path}")
    with open(path, mode='r', encoding='utf-8') as sf:
        return sf.readline().strip()

def write_config_file(args, config:str, name:str):
    """
    Write a tunnel config to a file.
    :param config: The wireguard interface config
    :param name: Config file name string
    :return: True if file written, else False
    """
    dst = Path(f'{CONFIG_PREFIX}/{name}.conf')
    logging.info(f"Writing config file to {dst}")

    if dst.is_file():
        if args.recreate:
            logging.info("File already exists, overwriting it as requested.")
        else:
            loggin.warning("File already exists, using the already present one instead.")
            return False

    with open(dst, mode='w', encoding='utf-8') as out:
        out.write(config)
        return True

def show_status(args):
    """
    Show present interface status.
    """
    # Mangle output of `wg` into yaml
    wg_show_cmd = 'wg show'
    show_res = subprocess.run(wg_show_cmd, capture_output=True, shell=True)
    wg_res_yaml = re.sub(to_underscore_pattern, '_', show_res.stdout.decode('utf-8')).replace('  ', '')
    wg_dict = yaml.safe_load(wg_res_yaml)

    # Identify name of ingress+egress enpoints
    servers = read_server_file(args.server_file_path)
    endpoint_ip = wg_dict['endpoint'].split(':')[0]
    endpoint_port = wg_dict['endpoint'].split(':')[1]
    ingress, egress = None, None
    for country in servers['countries']:
        for city in country['cities']:
            for relay in city['relays']:
                if relay['ipv4_addr_in'] == endpoint_ip:
                    ingress = Location(country['name'], city['name'])
                if str(relay['multihop_port']) == endpoint_port:
                    egress = Location(country['name'], city['name'])

    # Check connectivity
    ping_cmd = f'ping {PING_TARGET} -W 1 -c 1'
    ping_res = subprocess.run(ping_cmd, capture_output=True, shell=True)
    online = not ping_res.returncode

    # Print output
    status = InterfaceStatus(
        name = wg_dict['interface'],
        online = online,
        port = wg_dict['listening_port'],
        endpoint = endpoint_ip,
        ingress_loc = ingress,
        egress_loc = egress,
    )
    print(status)

def generate_wireguard_conf(args, privkey:str, ingress: dict, egress:dict) -> str:
    """
    Generate a wireguard config file
    :param args: Argparse args
    :param privkey: Wireguard client private key
    :param ingress: Ingress relay dict
    :param egress: Egress relay dict
    """
    logging.debug("Generating interface config file contents")

    killswitch = ""
    if not args.no_killswitch:
        logging.debug("Including killswitch")
        killswitch = KILLSWITCH

    conf = f"""[Interface]
PrivateKey = {privkey}
Address = 10.66.126.178/32,fc00:bbbb:bbbb:bb01::3:7eb1/128
DNS = 10.64.0.1
{killswitch}

[Peer]
PublicKey = {egress['public_key']}
Endpoint = {ingress['ipv4_addr_in']}:{egress['multihop_port']}
AllowedIPs = 0.0.0.0/0,::/0
"""

    return conf

def parse_args():
    """
    Do argparse.
    """
    parser = argparse.ArgumentParser(
        prog = "Switcharoo Mk2",
        description = "Military grade mullvad wireguard tunneler",
        epilog = "Happy tunneling"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action = 'store_true',
        help = "Display debug-level log messages"
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action = 'store_true',
        help = "Suppress stdout messages"
    )

    subparsers  = parser.add_subparsers()

    parser_up = subparsers.add_parser(
        'up',
        help = "Bring up interface, create new one if one already exists"
    )
    parser_up.set_defaults(
        func = bring_up,
        server_file_path = SERVER_FILE,
        privkey_path = PRIVATE_KEY_FILE
    )
    parser_up.add_argument(
        "-i",
        "--ingress",
        type = str,
        default = "random",
        help = "Multihop tunnel entrance country code. Default: Random"
    )
    parser_up.add_argument(
        "-e",
        "--egress",
        type = str,
        default = "random",
        help = "Multihop tunnel exit country code. Default: Random"
    )
    parser_up.add_argument(
        "-k",
        "--no-killswitch",
        action='store_true',
        help = "Stop network traffic if VPN connection drops. Default: Use killswitch"
    )
    parser_up.add_argument(
        "-r",
        "--recreate",
        type = bool,
        default = True,
        help = "Recreate config file if it exists. Default: True"
    )
    parser_down = subparsers.add_parser(
        'down',
        help = "Take down active interface"
    )
    parser_down.set_defaults(func=put_down)

    parser_update = subparsers.add_parser(
        "update",
        help = "Update server list"
    )
    parser_update.add_argument(
        "-o",
        "--output",
        type = str,
        help = "Write to this output file"
    )
    parser_update.set_defaults(func=update_server_list, server_file_path=SERVER_FILE)

    parser_show = subparsers.add_parser(
        'status',
        aliases = ['show'],
        help = "Show current status"
    )
    parser_show.set_defaults(func=show_status, server_file_path=SERVER_FILE)

    args = parser.parse_args()
    return args, parser

def main():
    # Setup
    random.seed()
    args, parser = parse_args()
    log_fmt = '[%(levelname)s] %(filename)s: %(message)s'
    if args.verbose:
        logging.basicConfig(format=log_fmt, level=logging.DEBUG)
    else:
        logging.basicConfig(format=log_fmt)
    logging.debug("Setup complete")

    # Act on subcommand
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()

if __name__ == '__main__':
    main()
